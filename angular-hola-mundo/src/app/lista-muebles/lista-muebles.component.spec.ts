import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMueblesComponent } from './lista-muebles.component';

describe('ListaMueblesComponent', () => {
  let component: ListaMueblesComponent;
  let fixture: ComponentFixture<ListaMueblesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMueblesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMueblesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
