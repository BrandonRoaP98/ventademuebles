import { Component, OnInit , Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-saludos',
  templateUrl: './saludos.component.html',
  styleUrls: ['./saludos.component.css']
})
export class SaludosComponent implements OnInit {
  @Input() saludo: DestinoViaje;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {}

  ngOnInit(): void {
  }

}
