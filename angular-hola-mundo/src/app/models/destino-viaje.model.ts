export class DestinoViaje {
    nombre:string;
    precio:number;
    imagenUrl:string;

    constructor(n:string ,p:number ,u:string){
        this.nombre = n;
        this.precio = p;
        this.imagenUrl = u;
    }
}