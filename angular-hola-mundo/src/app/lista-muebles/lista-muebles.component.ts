import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model'

@Component({
  selector: 'app-lista-muebles',
  templateUrl: './lista-muebles.component.html',
  styleUrls: ['./lista-muebles.component.css']
})
export class ListaMueblesComponent implements OnInit {
  muebles: DestinoViaje[];
  constructor() {
    this.muebles = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string ,precio:number ,url:string):boolean{
    this.muebles.push(new DestinoViaje(nombre,precio,url));
    
    return false;
  }

}
